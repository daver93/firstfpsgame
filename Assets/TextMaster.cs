﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextMaster : MonoBehaviour
{
    private int numberOfKills;
    public TextMeshProUGUI killsInfo;

    public void UpdateKillsTextInfo()
    {
        numberOfKills++;
        killsInfo.text = "Kills: " + numberOfKills;
    }
}
