﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Pistol : MonoBehaviour
{

    private Animator pistolAnimator;
    private AudioSource audio;

    public AudioClip pistolShootClip;

    public AudioClip[] pistolSounds;

    public GameObject bulletEmitter;
    public GameObject bullet;
    public float bulletForwardForce;

    public int numberOfBullets;

    public TextMeshProUGUI bulletInfo;

    // Start is called before the first frame update
    void Start()
    {
        pistolAnimator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();

        UpdateAmmoTextInfo(numberOfBullets);
    }

    private void Awake()
    {
        if (MainStaticScript.GetHasInfiniteAmmo())
        {
            numberOfBullets = -1;
        }
        else
        {
            numberOfBullets = 5;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!audio.isPlaying)
            {
                if (numberOfBullets == -1 || numberOfBullets > 0)
                {
                    //audio.clip = pistolShootClip;
                    audio.clip = pistolSounds[0];
                }
                else
                {
                    audio.clip = pistolSounds[1];
                }

                PistolShoot();
                UpdateAmmoTextInfo(numberOfBullets);
            }
        }
    }

    public void PistolShoot()
    {
        if (numberOfBullets > 0)
        {
            // If pistol has bullets left

            FireBullet();
            audio.Play();
            pistolAnimator.SetTrigger("shoot");

            numberOfBullets--;
        }
        else if (numberOfBullets == -1)
        {
            // If numberOfBullets is equal to -1, that means infinite number of bullets

            FireBullet();
            audio.Play();
            pistolAnimator.SetTrigger("shoot");
        }
        else
        {
            // Pistol is empty

            audio.Play();
        }
    }

    public void FireBullet()
    {
        //The Bullet instantiation happens here.
        GameObject Temporary_Bullet_Handler;
        Temporary_Bullet_Handler = Instantiate(bullet, bulletEmitter.transform.position, bulletEmitter.transform.rotation) as GameObject;

        //Sometimes bullets may appear rotated incorrectly due to the way its pivot was set from the original modeling package.
        //This is EASILY corrected here, you might have to rotate it from a different axis and or angle based on your particular mesh.
        Temporary_Bullet_Handler.transform.Rotate(Vector3.left * 90);

        //Retrieve the Rigidbody component from the instantiated Bullet and control it.
        Rigidbody Temporary_RigidBody;
        Temporary_RigidBody = Temporary_Bullet_Handler.GetComponent<Rigidbody>();

        //Tell the bullet to be "pushed" forward by an amount set by Bullet_Forward_Force.
        Temporary_RigidBody.AddForce(transform.forward * bulletForwardForce);

        //Basic Clean Up, set the Bullets to self destruct after n Seconds, I am being VERY generous here, normally 3 seconds is plenty.
        Destroy(Temporary_Bullet_Handler, 2.0f);
    }

    public void GetBullets(int numberOfNewBullets)
    {
        numberOfBullets += numberOfNewBullets;
        UpdateAmmoTextInfo(numberOfBullets);
    }

    public void UpdateAmmoTextInfo(int numberOfBullets)
    {
        if (numberOfBullets == -1)
        {
            bulletInfo.text = "Ammo: Infinite";
        }
        else
        {
            bulletInfo.text = "Ammo: " + numberOfBullets;
        }
    }
}
