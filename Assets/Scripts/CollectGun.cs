﻿using UnityEngine;

public class CollectGun : MonoBehaviour
{
    private GunMaster gunMaster;
    private SwapGuns swapGuns;

    private void Start()
    {
        gunMaster = GameObject.Find("GunMaster").GetComponent<GunMaster>();
        swapGuns = GameObject.FindGameObjectWithTag("Player").GetComponent<SwapGuns>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("Collect_Shotgun"))
        {
            // Destroy the collectable gun
            Destroy(other.gameObject);

            // Activate the gun in the hierarchy
            GameObject shotgun = GameObject.Find("MainCamera/GunPosition/Shotgun");
            shotgun.SetActive(true);

            // Store info in the Gun Master
            gunMaster.isShotgunAvailable = true;
            gunMaster.StoreNewGun(shotgun);

            swapGuns.ChangeActiveWeaponInCycle();
        }
    }
}
