﻿using System.Collections.Generic;
using UnityEngine;

public class GunMaster : MonoBehaviour
{
    public bool isPistolAvailable;
    public bool isShotgunAvailable;

    public bool isPistolEquipped;
    public bool isShotgunEquipped;

    public List<GameObject> guns;

    public SwapGuns swapGuns;

    private void Start()
    {
        isPistolAvailable = true;
        isShotgunAvailable = false;

        isPistolEquipped = true;
        isShotgunEquipped = false;

        GameObject parent = GameObject.Find("GunPosition");

        /*
        for (int i = 0; i < parent.gameObject.transform.childCount; i++)
        {
            guns.Add(parent.gameObject.transform.GetChild(i).gameObject);
        }
        */
        guns.Add(parent.gameObject.transform.GetChild(0).gameObject);

        swapGuns = GameObject.FindGameObjectWithTag("Player").GetComponent<SwapGuns>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            swapGuns.ChangeActiveWeaponInCycle();
        }
    }

    public void StoreNewGun(GameObject newGun)
    {
        guns.Add(newGun.gameObject);
    }
}
