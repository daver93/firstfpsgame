﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour
{
    public bool playerIsAlive;

    private void Start()
    {
        playerIsAlive = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Zombie"))
        {

            try
            {
                ZombieBehaviour zombieBehaviour = collision.gameObject.GetComponent<ZombieBehaviour>();

                if (zombieBehaviour.isAlive)
                {
                    playerIsAlive = false;

                    Time.timeScale = .5f;

                    StartCoroutine(FreezeForSeconds());
                }

            }
            catch (System.Exception e)
            {

            }
        }
    }

    IEnumerator FreezeForSeconds()
    {
        yield return new WaitForSeconds(1);
        Time.timeScale = 1;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
