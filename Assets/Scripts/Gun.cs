﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Gun : MonoBehaviour
{
    private Animator gunAnimator;
    private AudioSource audio;

    public int damage = 10;
    public float range = 1000f;
    public float fireRate = 3000f;
    public float impactForce = 30f;

    public int numberOfBullets;
    public TextMeshProUGUI bulletInfo;
    public AudioClip[] pistolSounds;

    public Camera camera;
    //public ParticleSystem muzzleFlash;
    public GameObject impactEffect;

    private float nextTimeToFire = 0f;

    public static List<string> enemies = new List<string> { "Zombie" };

    void Awake()
    {
        if (MainStaticScript.GetHasInfiniteAmmo())
        {
            numberOfBullets = -1;
        }
        else
        {
            numberOfBullets = 500;
        }
    }

    void Start()
    {
        gunAnimator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();

        UpdateAmmoTextInfo(numberOfBullets);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Time.time >= nextTimeToFire && !audio.isPlaying)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            try
            {
                if (numberOfBullets == -1 || numberOfBullets > 0)
                {
                    audio.clip = pistolSounds[0];
                    Shoot();
                    UpdateAmmoTextInfo(numberOfBullets);
                }
                else
                {
                    audio.clip = pistolSounds[1];
                }
                audio.Play();
            }
            catch (Exception e)
            {

            }
        }
    }

    void Shoot()
    {
        //muzzleFlash.Play();
        try
        {
            gunAnimator.SetTrigger("shoot");
        }
        catch (Exception e)
        {

        }

        if (numberOfBullets > 0)
        {
            numberOfBullets--;
        }

        RaycastHit hit;

        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, range))
        {
            if (enemies.Contains(hit.collider.gameObject.tag))
            {
                if (hit.collider.gameObject.tag.Equals("Zombie")){
                    ZombieBehaviour zombieBehaviour =  hit.collider.gameObject.GetComponent<ZombieBehaviour>();

                    if (zombieBehaviour.isAlive)
                    {
                        //zombieBehaviour.GetDamaged();
                        zombieBehaviour.GetDamaged(damage);
                    }
                }
            }

            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }

            GameObject impactGameObject = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impactGameObject, 0.1f);
        }
    }

    public void UpdateAmmoTextInfo(int numberOfBullets)
    {
        if (numberOfBullets == -1)
        {
            bulletInfo.text = "Ammo: Infinite";
        }
        else
        {
            bulletInfo.text = "Ammo: " + numberOfBullets;
        }
    }

    public void GetBullets(int numberOfNewBullets)
    {
        numberOfBullets += numberOfNewBullets;

        if (gameObject.activeInHierarchy)
        {
            UpdateAmmoTextInfo(numberOfBullets);
        }
    }
    
}
