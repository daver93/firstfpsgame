﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolAmmo : MonoBehaviour
{
    private Gun pistolGun;
    public Gun shotGun;

    private void Start()
    {
        pistolGun = GameObject.FindGameObjectWithTag("Pistol").GetComponent<Gun>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            if (gameObject.tag.Equals("PistolAmmo"))
            {
                if (Input.GetMouseButtonDown(1))
                {
                    Debug.Log("Collected Pistol Ammo");

                    // Pass the number of children as number of ammo
                    pistolGun.GetBullets(transform.childCount);

                    Destroy(gameObject);
                }
            }
            else if (gameObject.tag.Equals("ShotgunAmmo"))
            {
                if (Input.GetMouseButtonDown(1) && shotGun != null)
                {
                    Debug.Log("Collected Shotgun Ammo");

                    // Pass the number of children as number of ammo
                    shotGun.GetBullets(transform.childCount);

                    Destroy(gameObject);
                }
            }
        }
    }
}
