﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public TextMeshProUGUI specialTextInfo;

    public void PlayButtonPressed()
    {
        // Check for special command
        GetSpecialTextCommand();

        // Load next scene
        SceneManager.LoadScene("GameScene");
    }

    public void QuitButtonPressed()
    {
        Application.Quit();
    }

    public void GetSpecialTextCommand()
    {
        // If the text is correct, then player must have infinite number of ammo

        if (specialTextInfo.text.Contains("David Ammo"))
        {
            Debug.Log("Infinite ammo should be generated");
            MainStaticScript.SetHasInfiniteAmmo(true);
        }
        else
        {
            MainStaticScript.SetHasInfiniteAmmo(false);
        }

    }
}
