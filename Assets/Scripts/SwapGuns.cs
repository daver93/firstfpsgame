﻿using System;
using UnityEngine;

public class SwapGuns : MonoBehaviour
{
    private GunMaster gunMaster;
    private GameObject activeGun;

    private void Start()
    {
        gunMaster = GameObject.Find("GunMaster").GetComponent<GunMaster>();
    }

    public void ChangeActiveWeaponInCycle()
    {
        GameObject temp = gunMaster.guns[0];

        for (int i = 0; i < gunMaster.guns.Count; i++)
        {
            // If it's the last gun in the list
            if (i == gunMaster.guns.Count - 1)
            {
                if (gunMaster.guns[i].activeInHierarchy)
                {
                    gunMaster.guns[i].SetActive(false);
                    gunMaster.guns[0].SetActive(true);

                    activeGun = gunMaster.guns[0];

                    break;
                }
            }

            else if (gunMaster.guns[i].activeInHierarchy)
            {
                gunMaster.guns[i].gameObject.SetActive(false);
                gunMaster.guns[i+1].gameObject.SetActive(true);

                activeGun = gunMaster.guns[i+1];

                break;
            }
        }

        if (activeGun != null)
        {
            try
            {
                Gun gun = activeGun.gameObject.GetComponent<Gun>();
                gun.UpdateAmmoTextInfo(gun.numberOfBullets);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
    }
}
