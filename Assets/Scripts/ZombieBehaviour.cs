﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class ZombieBehaviour : MonoBehaviour
{
    public Animator zombieAnimator;
    public GameObject target;

    public float speed;
    public bool canSeePlayer;

    public int hitNeedsToDie;
    public int numberOfSuccessfulHits;

    public bool isAlive;

    public float radius;

    public NavMeshAgent agent;
    public int layerMask;

    private GameObject player;

    public AISpawn aISpawn;

    private TextMaster textMaster;

    public int health;

    private void Awake()
    {
        textMaster = GameObject.Find("TextMaster").GetComponent<TextMaster>();

        health = Random.Range(25, 50);
    }

    // Start is called before the first frame update
    void Start()
    {
        zombieAnimator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        target = GameObject.Find("Target");

        radius = 10;

        isAlive = true;

        aISpawn = GetComponent<AISpawn>();

        // How many times a bullet hitted enemy
        numberOfSuccessfulHits = 0;
        
        agent = GetComponent<NavMeshAgent>();

        // Disabling auto-braking allows for continuous movement
        // between points (ie, the agent doesn't slow down as it
        // approaches a destination point).
        agent.autoBraking = false;

        // Bit shift the index of the layer (8) to get a bit mask
        layerMask = 1 << 10;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            Scan2();
            LookAtThePlayer();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isAlive) {
            if (collision.gameObject.tag.Equals("Bullet"))
            {
                numberOfSuccessfulHits++;

                if (numberOfSuccessfulHits >= hitNeedsToDie)
                {
                    Debug.Log("Must be killed now");
                    Die();
                }
                else
                {
                    agent.isStopped = true;
                    zombieAnimator.SetTrigger("head_hitted");
                }

                Destroy(collision.gameObject);
            }
        }
    }

    public void GetDamaged()
    {
        numberOfSuccessfulHits++;

        if (numberOfSuccessfulHits >= hitNeedsToDie)
        {
            Debug.Log("Must be killed now");
            Die();
        }
        else
        {
            agent.isStopped = true;
            agent.destination = gameObject.transform.position;
            zombieAnimator.SetTrigger("head_hitted");
        }
    }

    public void GetDamaged(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Debug.Log("Must be killed now");
            Die();
        }
        else
        {
            agent.isStopped = true;
            agent.destination = gameObject.transform.position;
            zombieAnimator.SetTrigger("head_hitted");
        }
    }

    public void Die()
    {
        zombieAnimator.SetBool("died", true);
        isAlive = false;
        textMaster.UpdateKillsTextInfo();

        foreach (Collider c in GetComponents<Collider>())
        {
            c.enabled = false;
            Destroy(c);
        }

        aISpawn.removeMe();

        StartCoroutine(DestroyAfterDie());
    }

    public void LookAtThePlayer()
    {
        //look at the player (but only in x, z axis)
        Vector3 targetPosition = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z);
        transform.LookAt(targetPosition);
    }

    public void SeekPlayer()
    {
        if (!zombieAnimator.GetCurrentAnimatorStateInfo(0).IsName("Head Hit"))
        {
            float step = speed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);

            if (agent.remainingDistance < 0.5f)
            {
                agent.isStopped = true;
            }
        }
        else
        {
            Debug.Log("Head Hit now");
        }
    }

    public float fieldOfViewRange = 90; // in degrees (I use n, this gives the enemy a vision of n * 2 degrees)
    public float minPlayerDetectDistance = 2; // the distance the player can come behind the enemy without being deteacted
    public float rayRange = Mathf.Infinity; // distance the enemy can "see" in front of him
    private Vector3 rayDirection = Vector3.zero;

    public void Scan2()
    {
        RaycastHit hit;
        rayDirection = player.transform.position - transform.position;

        float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);

        if ((Vector3.Angle(rayDirection, transform.forward)) < fieldOfViewRange){ // Detect if player is within the field of view
            if (Physics.Raycast(transform.position, rayDirection, out hit, 30.0f))
            {
                if (hit.transform.tag == "Player")
                {
                    LookAtThePlayer();
                    agent.destination = player.transform.position;
                    SeekPlayer();
                    zombieAnimator.SetBool("run", true);
                }
                else
                {
                    //Debug.Log("Can not see player");
                    zombieAnimator.SetBool("run", false);
                }
            }
        }
    }

    IEnumerator DestroyAfterDie()
    {
        yield return new WaitForSeconds(3);

        Destroy(gameObject);
    }
}
