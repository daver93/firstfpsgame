﻿using System;

public static class MainStaticScript
{
    private static bool hasInfiniteAmmo;

    public static void SetHasInfiniteAmmo(bool infinite)
    {
        hasInfiniteAmmo = infinite;
    }

    public static bool GetHasInfiniteAmmo()
    {
        return hasInfiniteAmmo;
    }
}
